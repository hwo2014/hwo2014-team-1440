solution "HwoBot"
	location "./buildscripts"
	configurations { "Debug", "Release" }
	project "Bot"
		kind "ConsoleApp"
		language "C#"
		targetname "bot"
		links { 
			"System",
			"System.Core",
			"./depends/Newtonsoft.Json",
			"./depends/Mono.GameMath",
			"Game"
		}
		configuration { }
			files { "./src/bot/**.cs" }
			vpaths { ["*"] = "./src/bot" }
		configuration "Debug"
			targetdir "./bin/debug"
			optimize "Off"
		configuration "Release"
			targetdir "./bin/release"
			optimize "Full"
	project "Game"
		kind "SharedLib"
		language "C#"
		targetname "game"
		links { 
			"System",
			"System.Core",
			"./depends/Newtonsoft.Json",
			"./depends/Mono.GameMath"
		}
		configuration { }
			files { "./src/game/**.cs" }
			vpaths { ["*"] = "./src/game" }
		configuration "Debug"
			targetdir "./bin/debug"
			optimize "Off"
		configuration "Release"
			targetdir "./bin/release"
			optimize "Full"
if _ACTION == "clean" then
	os.rmdir("buildscripts")
	os.rmdir("bin")
end
