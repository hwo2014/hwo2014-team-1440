public enum PieceKind
{
    Straight,
    Bend
}

public struct Piece
{
    public PieceKind Kind;
    public bool Switch;
    public float Length;
    public float Radius;
    public float Angle;
}