using System;
using System.Collections.Generic;
using System.Linq;
using Mono.GameMath;
using Newtonsoft.Json.Linq;

public class Track
{
    public string Id;
    public Piece[] Pieces;
    public Lane[] Lanes;
    public Car[] Cars; 
    public List<Car[]> CarPositions;
}

public static class TrackExtensions
{
    private static float LengthOfBend(float radius, float angle)
    {
        return MathHelper.ToRadians(Math.Abs(angle))*radius;
    }

    public static void Init(this Track track, JObject data)
    {
        track.Id = (string) data["race"]["track"]["id"];

        track.Pieces = data["race"]["track"]["pieces"]
            .Children<JObject>()
            .Select(x =>
            {
                var piece = new Piece {Switch = false};
                JToken value;
                if (x.TryGetValue("switch", out value)) piece.Switch = (bool) value;
                if (x.TryGetValue("length", out value))
                {
                    piece.Kind = PieceKind.Straight;
                    piece.Length = (float) value;
                }
                else
                {
                    piece.Kind = PieceKind.Bend;
                    piece.Radius = (float) x["radius"];
                    piece.Angle = (float) x["angle"];
                }
                return piece;
            })
            .ToArray();

        track.Lanes = data["race"]["track"]["lanes"]
            .Children<JObject>()
            .Select(x => new Lane
            {
                Index = (int) x["index"],
                DistanceFromCenter = (float) x["distanceFromCenter"]
            })
            .ToArray();

        track.Cars = data["race"]["cars"]
            .Children<JObject>()
            .Select(x => new Car
            {
                Id = {Name = (string) x["id"]["name"], Color = (string) x["id"]["color"]},
                Length = (float) x["dimensions"]["length"],
                Width = (float) x["dimensions"]["width"],
                GuideFlagPosition = (float) x["dimensions"]["guideFlagPosition"],
                Lap = -1
            })
            .ToArray();

        track.CarPositions = new List<Car[]>();
    }

    public static void AddCarPositions(this Track track, JArray data)
    {
        track.CarPositions.Add(
            data.Children<JObject>()
                .Select(x =>
                {
                    var carId = new CarId {Name = (string) x["id"]["name"], Color = (string) x["id"]["color"]};
                    var car = track.Cars.First(y => y.Id.Equals(carId));
                    car.Angle = (float) x["angle"];
                    car.PieceIndex = (int) x["piecePosition"]["pieceIndex"];
                    car.InPieceDistance = (float) x["piecePosition"]["inPieceDistance"];
                    car.StartLane = (int) x["piecePosition"]["lane"]["startLaneIndex"];
                    car.EndLane = (int) x["piecePosition"]["lane"]["endLaneIndex"];
                    car.Lap = (int) x["piecePosition"]["lap"];
                    if (car.Lap != -1)
                    {
                        if (track.CarPositions.Count == 0) return car;
                        var lastestPosition = track.CarPositions.Last().FirstOrDefault(y => y.Id.Equals(carId));
                        if (!lastestPosition.Id.Equals(carId)) return car;
                        if (lastestPosition.Lap == -1)
                        {
                            lastestPosition.PieceIndex = 0;
                            lastestPosition.InPieceDistance = 0;
                            lastestPosition.StartLane = 0;
                        }
                        var lastPieceIndex = lastestPosition.PieceIndex + lastestPosition.Lap*track.Pieces.Length;
                        var currentPieceIndex = car.PieceIndex + car.Lap*track.Pieces.Length;
                        car.Velocity = Enumerable
                            .Range(lastPieceIndex, currentPieceIndex - lastPieceIndex)
                            .Select(y =>
                            {
                                var pieceIdx = y%track.Pieces.Length;
                                var piece = track.Pieces[pieceIdx];
                                if (piece.Kind == PieceKind.Straight) return piece.Length;
                                return LengthOfBend(
                                    piece.Radius - Math.Sign(piece.Angle)*track.Lanes[lastestPosition.StartLane].DistanceFromCenter,
                                    piece.Angle);
                            })
                            .Sum();
                        car.Velocity += car.InPieceDistance;
                        car.Velocity -= lastestPosition.InPieceDistance;
                    }
                    return car;
                })
                .ToArray());
    }
}