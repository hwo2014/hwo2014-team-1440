using System;

public struct CarId : IEquatable<CarId>
{
    public string Name;
    public string Color;

    public bool Equals(CarId other)
    {
        return Name == other.Name && Color == other.Color;
    }
}

public struct Car
{
    public CarId Id;
    public float Length;
    public float Width;
    public float GuideFlagPosition;
    public float Angle;
    public float Velocity;
    public int PieceIndex;
    public float InPieceDistance;
    public int Lap;
    public int StartLane;
    public int EndLane;
}