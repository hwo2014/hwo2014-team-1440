using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;
        var track = new Track();

        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "carPositions":
                    {
                        track.AddCarPositions((JArray) msg.data);
                        var lastestPos0 = track.CarPositions[track.CarPositions.Count - 1].First();
                        if (lastestPos0.Lap == 0)
                        {
                            if (track.CarPositions.Count >= 2)
                            {
                                var lastestPos1 = track.CarPositions[track.CarPositions.Count - 2].First();
                                if (lastestPos0.PieceIndex == 2 && lastestPos1.PieceIndex == 1)
                                {
                                    send(new SwitchLane("Right"));
                                    continue;
                                }
                                if (lastestPos0.PieceIndex == 7 && lastestPos1.PieceIndex == 6)
                                {
                                    send(new SwitchLane("Left"));
                                    continue;
                                }
                                if (lastestPos0.PieceIndex == 17 && lastestPos1.PieceIndex == 16)
                                {
                                    send(new SwitchLane("Right"));
                                    continue;
                                }
                            }
                        }
                        if (track.CarPositions.Count >= 2)
                        {
                            var lastestPos1 = track.CarPositions[track.CarPositions.Count - 2].First();
                            if (lastestPos0.PieceIndex == 7 && lastestPos1.PieceIndex == 6)
                            {
                                send(new SwitchLane("Left"));
                                continue;
                            }
                            if (lastestPos0.PieceIndex == 17 && lastestPos1.PieceIndex == 16)
                            {
                                send(new SwitchLane("Right"));
                                continue;
                            }
                        }
                        var pieceNext = track.Pieces[(lastestPos0.PieceIndex + 1)%track.Pieces.Length];
                        if (pieceNext.Kind == PieceKind.Bend)
                        {
                            var factor = 45.0/Math.Abs(pieceNext.Angle);
                            send(
                                new Throttle(
                                    lastestPos0.Velocity > 6.5*factor ||
                                    Math.Abs(lastestPos0.Angle) > 50.0
                                        ? 0.0
                                        : 1.0));
                            continue;
                        }
                        send(new Throttle(1.0));
                    }
                    break;
                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    track.Init((JObject)msg.data);
                    Console.WriteLine("Race init");
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "crash":
                    {
                        var lastestPos = track.CarPositions.Last().First();
                        Console.WriteLine("Crashed a: {0}, v: {1}", lastestPos.Angle, lastestPos.Velocity);
                        send(new Ping());
                    }
                    break;
                default:
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}